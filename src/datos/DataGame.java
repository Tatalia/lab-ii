/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author nati2
 */
public class DataGame {
    public void saveResults(int[] player1 , int[] player2){
        File saveResult = new File("Save.txt");
        String winner = "";
        
        if(player1[4] > player2[4]){
            winner = "Jugador 1";
        }else if(player1[4] < player2[4]){
            winner = "Jugador 2";
        }else{
            winner = "Empate";
        }
 
        try {
            saveResult.createNewFile();
        }catch (Exception ex) {
            System.out.println("Error al crear el archivo Save:\n" + ex);
        }

        try{
            BufferedWriter save = new BufferedWriter(new FileWriter(saveResult, true));
            save.write(
                    "Número de Juego:" + "##" + "\n" + 
                    "Cantidad de puntos del Jugador 1: " + player1[4] + "\n" + 
                    "Cantidad de puntos del Jugador 2: " + player2[4] + "\n" +
                    "Ganador: " + winner + "\n");
            save.close();
        }catch (Exception ex) {
            System.out.println("Error al guardar el archivo Save:\n" + ex);
        }    
    }
    
        public String readingFileToShowValuesOnScreen(int index, String fileName) {
        File entityFile = new File(fileName);
        String valuesToShow = "";

        try {
            
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));

            ArrayList<String> entityArray = new ArrayList<>();

            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
         
            for (String singleEntity : entityArray) {
                //divido cada valor 
                String[] tempArray = singleEntity.split(",", 0);
                if (index == -1) {
                    valuesToShow += entityArray.indexOf(singleEntity) + 1 + " " + singleEntity + ":\n";
                } else {
                    valuesToShow += entityArray.indexOf(singleEntity) + 1 + " " + tempArray[index] + ":\n";
                }
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ":\n" + ex);
        }
        return valuesToShow;
    }
    
}
