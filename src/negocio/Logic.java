/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.DataGame;
/**
 *
 * @author nati2
 */
public class Logic {
    
    DataGame dataGame = new DataGame();
    
    public void savingGameRecords(int[] player1 , int[] player2){
        dataGame.saveResults(player1, player2);
    }
    
    public String readingGameRecords(int index, String fileName){
        String recordsToShow = dataGame.readingFileToShowValuesOnScreen(index, fileName);
        return recordsToShow;
    }
    
    public int randomNumberGenerator(int limitNumber){
        int randomResult = (int)Math.floor(Math.random()*limitNumber+1);
        return randomResult;
        
    }
    
    public int[] playProcess(int[] viewData){
        int[] result = new int[7];
        //int[] result = new int[6];
        
        //int randomNumber = randomNumberGenerator(4);
        int randomResult = (int)Math.floor(Math.random()*4+1);
        viewData[1] += 1;
        result = viewData;
        result[0] = viewData[0];
        
        if(viewData[2] == 1 || viewData[2] == 2 ){ //Valido entrada de estante
            if(viewData[1] == 5){ //Valido numero del balon para ver cuanto vale el punto
                if(viewData[1] == randomResult){ //Valido numero del balon contra random para ver si merece el punto
                    result[4] += 2;   
                    result[3] = 1; //lanzamiento exitoso +2 puntos
                }else{
                    result[4] = viewData[4];
                    result[3] = 0; //lanzamiento fallido
                }
                
            }else{
                if(viewData[1] == randomResult){
                    result[4] += 1; 
                    result[3] = 1; //lanzamiento exitoso +1 puntos
                }else{
                    result[4] = viewData[4];
                    result[3] = 0; //lanzamiento fallido
                }
            }           
        }else{//el valor de los puntos es 2
            if(viewData[1] == randomResult){ //Valido numero del balon contra random para ver si merece el punto
                result[4] += 2;  
                result[3] = 1; //lanzamiento exitoso +2 puntos
            }else{
                result[4] = viewData[4];
                result[3] = 0; //lanzamiento fallido
            }
        }
        
        if(viewData[2] == 3){ //validamos estante y lanzamiento para dar por finalizado el juego
            if(viewData[1] == 5){
                result[5] = 1; //asignamos uno a finalizado para indicar que termino
            }  
        }
        
        if(viewData[1] == 6){ //Si vamos en el lanzamiento num 5 entonces cambiar el estante al siguiente nivel y pasar balon de nuevo a 1
            result[2] += 1; //pasamos al siguiente estante
            viewData[1] = 1;
        } 
        
        viewData[6] = randomResult;
        
        return result;
    }
    
}
